# frozen_string_literal: true

Rails.application.config.middleware.use OmniAuth::Builder do
  OmniAuth.config.logger = Rails.logger
  # Remove built-in CSS for the login form
  OmniAuth.config.form_css = ""

  provider(
    OmniAuth::Strategies::LDAP,
    name: :ldap,
    default_custom_css_path: "/assets/ldap-login-form.css",
    setup: setup_provider_proc(:ldap,
      host: :host,
      port: :port,
      method: :method,
      base: :base,
      bind_dn: :bind_dn,
      filter: :filter,
      password: :password,
      custom_css_path: :custom_css_path,
      first_name: :first_name,
      last_name: :last_name,
      email: :email,
      phone: :phone,
      mobile: :mobile,
      nickname: :nickname,
      location: :location,
      uid: :uid,
      url: :url,
      image: :image,
      description: :description
    )
  )
end
