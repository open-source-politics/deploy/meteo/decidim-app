# OVERRIDE LIST

This files have been overridden from their origin repository

## decidim
> version : 0.22.0
> repo : https://github.com/decidim/decidim/tree/v0.22.0

- [app/controllers/concerns/decidim/force_authentication.rb](/app/controllers/concerns/decidim/force_authentication.rb)
- [app/views/decidim/system/organizations/_omniauth_provider.html.erb](/app/views/decidim/system/organizations/_omniauth_provider.html.erb)
- [app/views/layouts/decidim/_wrapper.html.erb](/app/views/layouts/decidim/_wrapper.html.erb)
