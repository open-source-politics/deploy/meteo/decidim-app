# frozen_string_literal: true

module OmniAuth
  module LDAP
    class CSRFForm < OmniAuth::Form

      def hidden_field(name, value)
        @html << "\n<input type='hidden' id='#{name}' name='#{name}' value='#{value}'/>"
        self
      end

    end
  end
end
