require 'omniauth'
require 'i18n'

module OmniAuth
  module Strategies
    class LDAP
      include OmniAuth::Strategy
      @@config = {
        'name' => 'cn',
        'first_name' => 'givenName',
        'last_name' => 'sn',
        'email' => ['mail', "email", 'userPrincipalName'],
        'phone' => ['telephoneNumber', 'homePhone', 'facsimileTelephoneNumber'],
        'mobile' => ['mobile', 'mobileTelephoneNumber'],
        'nickname' => ['uid', 'userid', 'sAMAccountName'],
        'title' => 'title',
        'location' => {"%0, %1, %2, %3 %4" => [['address', 'postalAddress', 'homePostalAddress', 'street', 'streetAddress'], ['l'], ['st'],['co'],['postOfficeBox']]},
        'uid' => 'dn',
        'url' => ['wwwhomepage'],
        'image' => 'jpegPhoto',
        'description' => 'description'
      }
      option :title, "LDAP Authentication" #default title for authentication form
      option :port, 389
      option :method, :plain
      option :uid, 'sAMAccountName'
      option :default_custom_css_path, nil
      option :custom_css_path, nil
      option :name_proc, lambda {|n| n}

      def request_phase
        return redirect('/users/sign_in') if session["warden.user.user.key"].present?

        OmniAuth::LDAP::Adaptor.validate @options

        I18n.locale = request["locale"].presence || session["user_locale"] || I18n.default_locale
        csrf_token = request["authenticity_token"].presence || session["_csrf_token"]

        extra_headers = [
          "\n<meta name='csrf-param' content='authenticity_token'>",
          "\n<meta name='csrf-token' content='#{csrf_token}'>"
        ]

        if options[:custom_css_path].present? || options[:default_custom_css_path].present?
          extra_headers.push("\n<link href='#{options[:custom_css_path].presence || options[:default_custom_css_path]}' media='screen' rel='stylesheet' />")
        end

        f = OmniAuth::LDAP::CSRFForm.new(:title => I18n.t('.title', scope: 'omniauth.ldap.form', default: 'LDAP Authentication'), :url => callback_path, :header_info => extra_headers.join )
        f.text_field I18n.t('.username', scope: 'omniauth.ldap.form', default: 'Login'), 'username'
        f.password_field I18n.t('.password', scope: 'omniauth.ldap.form', default: 'Password'), 'password'
        f.hidden_field 'utf8', '✓'
        f.hidden_field 'authenticity_token', csrf_token
        f.button I18n.t('.login', scope: 'omniauth.ldap.form')
        f.to_response
      end

      def callback_phase
        @adaptor = OmniAuth::LDAP::Adaptor.new @options

        return fail!(:missing_credentials) if missing_credentials?
        begin
          @ldap_user_info = @adaptor.bind_as(:filter => filter(@adaptor), :size => 1, :password => request['password'])
          return fail!(:invalid_credentials) if !@ldap_user_info

          @user_info = self.class.map_user(@@config, @ldap_user_info)
          super
        rescue Exception => e
          return fail!(:ldap_error, e)
        end
      end

      def filter adaptor
        if adaptor.filter and !adaptor.filter.empty?
          Net::LDAP::Filter.construct(adaptor.filter % {username: @options[:name_proc].call(request['username'])})
        else
          Net::LDAP::Filter.eq(adaptor.uid, @options[:name_proc].call(request['username']))
        end
      end

      uid {
        @user_info["uid"]
      }
      info {
        @user_info
      }
      extra {
        { :raw_info => @ldap_user_info }
      }

      def self.map_user(mapper, object)
        user = {}
        mapper.each do |key, value|
          case value
          when String
            user[key] = object[value.downcase.to_sym].first if object.respond_to? value.downcase.to_sym
          when Array
            value.each {|v| (user[key] = object[v.downcase.to_sym].first; break;) if object.respond_to? v.downcase.to_sym}
          when Hash
            value.map do |key1, value1|
              pattern = key1.dup
              value1.each_with_index do |v,i|
                part = ''; v.collect(&:downcase).collect(&:to_sym).each {|v1| (part = object[v1].first; break;) if object.respond_to? v1}
                pattern.gsub!("%#{i}",part||'')
              end
              user[key] = pattern
            end
          end
        end
        user
      end

      protected

      def missing_credentials?
        request['username'].nil? or request['username'].empty? or request['password'].nil? or request['password'].empty?
      end # missing_credentials?
    end
  end
end

OmniAuth.config.add_camelization 'ldap', 'LDAP'
